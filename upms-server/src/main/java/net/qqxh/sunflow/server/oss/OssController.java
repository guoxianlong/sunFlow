package net.qqxh.sunflow.server.oss;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import net.qqxh.common.ossclient.FolderType;
import net.qqxh.common.ossclient.MagicOssClient;
import net.qqxh.sunflow.server.common.BaseController;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URL;
import java.util.Scanner;

@RestController
@RequestMapping("/oss")
public class OssController extends BaseController {
    @Autowired
    private MagicOssClient magicOssClient;

    @RequestMapping("/put")
    public Object upload(@RequestParam("file") MultipartFile file,String fix) {
        String url = "";
        try {
            url = magicOssClient.upload("upms", "userAvatar", FolderType.yyyyMM, file.getInputStream(),fix);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return responseSuccess(url);
    }

    @RequestMapping("/upload/{biz}")
    public Object upload(@PathVariable String biz, @RequestParam("file") MultipartFile file) {
        String url = "";
        try {
            String originalFilename = file.getOriginalFilename();
            url = magicOssClient.upload("sunflow", biz, FolderType.yyyyMMdd, file.getInputStream(), originalFilename.substring(originalFilename.lastIndexOf(".")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(StringUtils.isNotEmpty(url)) {
            return responseSuccess(url);
        } else {
            return responseFail("上传至oss失败");
        }
    }
}
